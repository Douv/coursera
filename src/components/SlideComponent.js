import React from 'react';
import { UncontrolledCarousel } from 'reactstrap';

const items = [
    {
        src: 'assets/images/duri.jpg',
        altText: 'Slide 1',
        caption: 'Slide 1',
        header: 'Slide 1 Header',
        key: '1'
    },
    {
        src: 'assets/images/griot.jpg',
        altText: 'Slide 1',
        caption: 'Slide 1',
        header: 'Slide 1 Header',
        key: '1'
      },
    {
    src: 'assets/images/cap deli.jpg',
    altText: 'Slide 1',
    caption: 'Slide 1',
    header: 'Slide 1 Header',
    key: '1'
  },
  {
    src: 'assets/images/pizza.jpg',
    altText: 'Slide 2',
    caption: 'Slide 2',
    header: 'Slide 2 Header',
    key: '2'
  },
  {
    src: 'assets/images/pizza2.jpg',
    altText: 'Slide 3',
    caption: 'Slide 3',
    header: 'Slide 3 Header',
    key: '3'
  },
  {
    src: 'assets/images/dulce.jpg',
    altText: 'Slide 4',
    caption: 'Slide 4',
    header: 'Slide 3 Header',
    key: '3'
  },
];

const Example = () => <UncontrolledCarousel items={items} />;

export default Example;